### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ cf3f2b19-a4e2-4488-aee1-dcac8e537a56
@quickactivate "outer"

# ╔═╡ 38720024-e8fb-11eb-0a06-596980796b12
using DrWatson

# ╔═╡ 2e6fc0cc-ccbb-4240-83de-e1052f0d0efd
using LightGraphs, Outerplanar, GraphPlot

# ╔═╡ 817e0de2-ec08-4af5-a436-ecc7468e7fdf
md"""
# Gráficas outerplanares

Una gráfica es outerplanar si se puede embedir en el plano con todos sus vértices en la cara exterior.
"""

# ╔═╡ a06ee2bc-76e5-41a2-9d25-1b9306ab2443
begin
	g = two_reductible_graph(30, seed=1)
	nv(g)
end

# ╔═╡ Cell order:
# ╠═38720024-e8fb-11eb-0a06-596980796b12
# ╠═cf3f2b19-a4e2-4488-aee1-dcac8e537a56
# ╠═2e6fc0cc-ccbb-4240-83de-e1052f0d0efd
# ╟─817e0de2-ec08-4af5-a436-ecc7468e7fdf
# ╠═a06ee2bc-76e5-41a2-9d25-1b9306ab2443
