using Test, Outerplanar, LightGraphs, Random
import Outerplanar:add_random_adj!
import Outerplanar:split_edge!

rng = MersenneTwister(0)

@testset "Expantion" begin
    n = rand(rng, 1:100, 2)
    g = SimpleGraph(n[1], n[2])
    add_random_adj!(g, 1, rng)
	@test ne(g) == n[2] + 1

    g = SimpleGraph(2)
    add_edge!(g, 1, 2)
    e = collect(edges(g))[1]
    split_edge!(g, e)
    @test nv(g) == 3
    @test has_edge(g, 1, 3)
    @test has_edge(g, 3, 2)
end

@testset "2-reductible" begin
    n = rand(rng, 1:100)
    @test nv(two_reductible_graph(n)) == n

end

@testset "Outerplanar" begin
    @test nv(outerplanar_graph(0)) == 0
    @test ne(outerplanar_graph(0)) == 0
end
