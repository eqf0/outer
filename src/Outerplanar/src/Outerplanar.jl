module Outerplanar

using LightGraphs, Random

export two_reductible_graph, outerplanar_graph

@enum ExpantionType begin
    new_v
    join_new_v
    expand_edge
    triangle
end

function add_random_adj!(g, n, rng)
    noadj = [
        v for v in vertices(g) if !has_edge(g, n, v) && n != v
    ]
    if length(noadj) == 0
        return false
    end
    v = rand(rng, noadj)
    add_edge!(g, n, v)
end

function split_random_edge!(g, rng, rem=false)
    if ne(g) == 0
        return false
    end
    e = rand(rng, collect(edges(g)))
    split_edge!(g, e, rem)
end

function split_edge!(g, e, rem=false)
    if rem && !rem_edge!(g, e)
        return false
    end

    u, v = src(e), dst(e)
    if !add_vertex!(g)
        return false
    end

    n = nv(g)

    if !add_edge!(g, u, n) || !add_edge!(g, n, v)
        rem_vertex!(g, n)
        return false
    end
    true
end

function two_reductible_graph(n::Integer; seed=0)
    g = SimpleGraph()
    if n <= 0
        return g
    end
    rng = MersenneTwister(seed)
    add_vertex!(g)
    for _ in 1:n - 1
        # adding a vertex is always a possibility
        # disabled disconneted vertices to get connected graphs
        steps = [join_new_v]# [new_v, join_new_v]

        # only split edges if there are any
        # both splits keep the graph connected, so both are present
        if ne(g) > 0
            append!(steps, [expand_edge, triangle])
        end

        # TODO: use some kind of user provided distribution?
        step = rand(rng, steps)

        if step == new_v || step == join_new_v
            add_vertex!(g)
            if step == join_new_v
                add_random_adj!(g, nv(g), rng)
            end
        else
            split_random_edge!(g, rng, (step == expand_edge))
        end
    end
    g
end

"""
    outerplanar(nv)

Creates undirected
[outerplanar graph](https://en.wikipedia.org/wiki/Outerplanar_graph) with `n`
 vertices and random edges.
"""
    function outerplanar_graph(n::Integer; seed=0)
        rng = MersenneTwister(seed)
        g = SimpleGraph()

        for _ in 1:n
            continue
        end
        g
    end
end # module
